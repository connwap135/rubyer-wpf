﻿using System.ComponentModel;

namespace Rubyer.Enums
{
    /// <summary>
    /// 头像形状
    /// </summary>
    public enum AvatarShape
    {
        /// <summary>
        /// 圆角
        /// </summary>
        [Description("圆角")]
        Round,

        /// <summary>
        /// 圆形
        /// </summary>
        [Description("圆形")]
        Circle
    }
}
